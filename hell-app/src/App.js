import './Styles/App.css';
import Counter from './Composants/Counter'

function App() {
  return (
    <div className="App">
      <Counter />
    </div>
  );
}

export default App;
