import React, { useEffect, useMemo, useState } from "react";
import Brick from "./Brick";
import './../Styles/Counter.css';

function Counter() {

    //  Counter is a state initialized to 0
    // const [brickCounter, setBrickCounter] = useState(0);
    const [flights, setFligths] = useState([]);
    const [reload, setReload] = useState(0);

    // const bricks = useMemo(() => {
    //     const tempBricks = [];
    //     for(let i = 0; i < brickCounter; i++){
    //         tempBricks.push(<Brick />);
    //     }
    //     return tempBricks;
    // }, [brickCounter]);
    useEffect(() => {
        fetch('https://data.vatsim.net/v3/vatsim-data.json')
        .then(res => res.json())
        .then((json) => {
            console.log(json);
            setFligths(json.pilots);
        })
        .catch((err) => {
            console.error(err);
        })
    }, [reload]);
    
    // // Function is called everytime increment button is clicked
    // const add = () => setBrickCounter(brickCounter + 1);
    
    // // Function is called everytime decrement button is clicked
    // const remove = () => setBrickCounter(brickCounter - 1);
    
    const [callsignSearchInputValue, setCallsignSearchInputValue] = useState('');

    const filteredFlights = useMemo(() => 
        flights.filter(flight => flight.callsign.includes(callsignSearchInputValue)), 
    [flights, callsignSearchInputValue])

    return (
        <div className="Counter">
            {/* <div className="CounterBrick">
                <button onClick={remove}>-1</button>
                <p> Il y a {brickCounter} brique(s)</p>
                <button onClick={add}>+1</button>
            </div>
            {bricks} */}
            <div className="CounterBrick">
                <button onClick={() => setReload(reload + 1)} >Refresh Data</button>
                <input type="text" value={callsignSearchInputValue} onChange={(event) => setCallsignSearchInputValue(event.target.value)}/>
            </div>
            {flights.length}
            {filteredFlights.map((item, index) => {
                const departure = item.flight_plan ? item.flight_plan.departure : 'non connu';
                const arrival = item.flight_plan ? item.flight_plan.arrival : 'non connu';
                return <Brick key={index} callsign={item.callsign} origin={departure} destination={arrival} />
            })}
        </div>
    );
}
  
export default Counter;