import './../Styles/Brick.css';

function Brick({callsign, origin, destination}) {

    return (
        <div className="Brick">
            <p>Numéro de vol : {callsign}</p>
            <p>Origine : {origin}</p>
            <p>Destination : {destination}</p>
        </div>
    );
}
  
export default Brick;